Exercise 1:1:
ls -l && ls -ld .?*
ls -> Lists information about FILES and DIRECTORIES and their contents
-l -> lists the information in a list format
-d -> lists directories and their contents
.?* -> lists all directories which are normally hidden

on the first column you see the permissions of each directory or file (owner, group and others)
first 4 digits maximum of 2² permissions (drwxr) -> directory, read, write, execute and read
second 3 digits of group (rwx) -> read , write and execute

executable files doesn't have any suffix in the file like in windows .exe or something. Depending on the OS,
the files are mostly .rpm .AppImage etc. -> to install the given applications

hidden files in linux starts with . and in windows mostly with $

chmod is for changing permissions with given bits (777) means EVERY permission is given to ANY user or group

grep is for searching PATTERN in each FILE

Exercise 1.2:

mkdir -p foo/bar

Normally, you are not allowed to delete non-empty directories with rmdir -> a error would be thrown.
You need to use rm -R (recursive) to delete non-empty directories without any error

For copying files you use cp with cp FILENAME SOURCE DESTINATION
for moving files it is quite similar with mv FILENAME SOURCE DESTINATION

Exercise 1:3:
printf "%30s E\n" >> new.txt 

in normal order : cat new.txt
in reverse order: tac new.txt
first 3 lines: head -3 new.txt
first 3 lines in normal order: cat new.txt | head -3
first 3 lines in reverse order: tac new.txt | head -3
last 3 lines: cat new.txt | tail -3
last 3 lines in normal order: cat new.txt | tail -3
last 3 lines in reverse order: tac new.txt | tail -3
