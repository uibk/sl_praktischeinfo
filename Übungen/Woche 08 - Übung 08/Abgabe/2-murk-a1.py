import random


def init_array_with_random_numbers(array, arraySize, randomRange):
    i = 0
    while(i <= arraySize):
        array.append(random.randint(-randomRange, randomRange))
        i+=1
    return array