# Author: Hamid Faragardi

import random,time


def max_subarray_sum_3(n, a):
    max = 0
    for i in range(0, n):
        for j in range(i, n):
            sum = 0
            for k in range(i, j+1):
                sum = sum + a[k]
            if(sum > max):
                max = sum
    return max


def max_subarray_sum_2(n,a):
    max = 0
    for i in range(0,n):
        sum = 0
        for j in range(i,n):
            sum = sum + a[j]
            if(sum>max):
                max = sum
    return max


def max_subarray_sum_1(n,a):
    max = 0
    sum = 0
    for i in range(0,n):
        sum = sum + a[i]
        if(sum > 0):
            if(sum > max):
                max = sum
        else:
            sum = 0
    return max


def random_array(n,z):
    n_array = []
    for number in range(1,n):
        n_array.append(random.randint(-z,z))
    print("The random array is: ", n_array )
    return n_array

start_time = time.time()
z = int(input("Please Enter z"))
n = [int(input("Please Enter n"))]

for number in n:
    array = random_array(number,z)
    start_time_1 = time.time()
    max_subarray_sum_1(n=number-1, a=array)
    end_time_1 = time.time()
    start_time_2 = time.time()
    max_subarray_sum_2(n=number-1, a=array)
    end_time_2 = time.time()
    start_time_3 = time.time()
    max_subarray_sum_3(n=number-1, a=array)
    end_time_3 = time.time()
    print("The execution time of array with array of "+str(number)+" elements for o(n) is: ", round(end_time_1 - start_time_1,5))
    print("The execution time of array with array of "+str(number)+" elements for o(n^2) is: ", round(end_time_2 - start_time_2,5))
    print("The execution time of array with array of "+str(number)+" elements for o(n^3) is: ", round(end_time_3 - start_time_3,5))
    print("\n")
print("The program duration is :", round(time.time() - start_time, 5))
