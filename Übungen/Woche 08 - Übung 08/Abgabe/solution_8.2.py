# Author: Hamid Faragardi

import random,time


def hasDuplicates_2(n, a):
    duplicates = []
    b = False
    i = 0
    while i < n - 1:
        j = i + 1
        while j < n:
            if(a[i] == a[j]):
                duplicates.append(a[i])
                b = True
            j = j + 1
        i = i +1
    print("The duplicate array is: ", duplicates)
    return b


def hasDuplicates_1(n, a, m):
    duplicates = []
    h = {}
    for i in range(-2**(m-1), 2**(m-1)-1):
        h[i] = 0
    b = False
    i = 0
    while i < n:
        if(h[a[i]] != 0):
            b = True
            duplicates.append(a[i])
        else:
            h[a[i]] = 1
        i = i + 1
    print("The duplicate array is: ",duplicates)
    return b


def random_array(n,z):
    n_array = []
    for number in range(1,n):
        n_array.append(random.randint(-z,z))
    print("The random array is: ", n_array )
    return n_array

start_time = time.time()

z = int(input("Please Enter z"))

n = [int(input("Please Enter n"))]

for number in n:
    array = random_array(number,z)
    start_time_1 = time.time()
    hasDuplicates_1(n=number-1, a=array , m=12)
    end_time_1 = time.time()
    start_time_2 = time.time()
    hasDuplicates_2(n=number-1, a=array)
    end_time_2 = time.time()
    print("The execution time of array with array of "+str(number)+" elements for o(n) is: ", round(end_time_1 - start_time_1,5))
    print("The execution time of array with array of "+str(number)+" elements for o(n^2) is: ", round(end_time_2 - start_time_2,5))
    print("\n")
print("The program duration is :", round(time.time() - start_time, 5))
