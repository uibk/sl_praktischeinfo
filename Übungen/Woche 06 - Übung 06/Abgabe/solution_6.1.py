# Author: Hamid Faragardi

number_1 = int(input("Please Enter number_1"))
number_2 = int(input("Please Enter number_2"))

number_1_divisor = []
number_2_divisor = []
sum_number_1_divisor = 0
sum_number_2_divisor = 0

for divisor in range(1,round(number_1/2)+1):
    if number_1 % divisor == 0:
        sum_number_1_divisor = sum_number_1_divisor + divisor

for divisor in range(1,round(number_2/2)+1):
    if number_2 % divisor == 0:
        sum_number_2_divisor = sum_number_2_divisor + divisor

if (sum_number_2_divisor == number_1) and (sum_number_1_divisor == number_2):
    print(str(number_1) + " and " + str(number_2) + " are amicable.")
else:
    print(str(number_1) + " and " + str(number_2) + " are not amicable.")
