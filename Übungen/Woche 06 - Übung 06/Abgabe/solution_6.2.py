# Author: Hamid Faragardi

import random


n = int(input("Please Enter N: "))
z = int(input("Please Enter z: "))


def first_array_implement(n):
    n_array = []
    for number in range(0,n):
        n_array.append(number)
    print("The first array is:" ,n_array)

def second_array_implement(n,z):
    n_array = []
    for number in range(1,n):
        n_array.append(random.randint(-z,z))
    print("The second array is: ", n_array)

first_array_implement(n)
second_array_implement(n,z)