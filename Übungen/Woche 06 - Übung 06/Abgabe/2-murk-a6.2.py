import random

randomRange = 5
arraySize = 10
array = []


def init_array_with_numbers(array, arraySize):
    for i in range(0, arraySize):
        array.append(i)
    return array


def init_array_with_random_numbers(array, arraySize, randomRange):
    for x in range(0, arraySize):
        array.append(random.randint(-randomRange, randomRange))
    return array


print(str(init_array_with_numbers(array, arraySize)))

array.clear()

print(str(init_array_with_random_numbers(array, arraySize, randomRange)))

# for adding elements in array array.append
# for removing elements in array array.remove
# for reading elements in array array.get
