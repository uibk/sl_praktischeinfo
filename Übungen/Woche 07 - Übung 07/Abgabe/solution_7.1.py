# Author: Hamid Faragardi

import random
n= int(input("Please Enter n"))
int_n = 0
for i in range (0,n):
    fp_one = random.uniform(0, 1)
    fp_two = random.uniform(0, 1)
    fp_one_squared = fp_one * fp_one
    fp_two_squared = fp_two * fp_two
    sum = fp_one_squared + fp_two_squared
    if sum <= 1:
        int_n = int_n + 1
float_n = float(int_n)
output = float_n
print(output)
