x = 0.0
limit = int(input("Tell me limit:"))
i = 0

while i <= limit:
    x += 0.1
    i += 1

print("%.5f" % x)

# expectation was that x is same as limit divided by 10 but there is a slight difference
# instead of limit = 50000 and x = 5000 , x is 5000.1 due to float operation, I suppose