import random
import time
import sys

def sort_bubble(numbers):
    length = len(numbers)
    for i in range(length):
        for j in range(length -i - 1):
            if numbers[j] > numbers[j + 1]:
                numbers[j], numbers[j + 1] = numbers[j + 1], numbers[j]


def sort_bucket(arr, num_buckets, func):
    buckets = [list() for _ in range(num_buckets)]
    for v in arr:
        buckets[int(func(v)*(num_buckets - 1))].append(v) # find bucket, here: equal subdivision of the value range
        arr.clear()
        for bucket in buckets:
            sort_bubble(bucket) # bucket.sort()
            arr.extend(bucket)

def time_func(func):
    start = time.perf_counter()
    ret = func()
    end = time.perf_counter()
    return ret, end - start