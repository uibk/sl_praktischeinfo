import random
import time
import sys


def search_linear(numbers, value):
    for i in range(0, len(numbers)):
        if numbers[i] == value:
            return i

    return -1


def search_binary(numbers, value):
    min = 0
    max = len(numbers) - 1

    while min <= max:
        i = min + (max - min) // 2

        if numbers[i] == value:
            return i
        elif numbers[i] < value:
            min = i + 1
        else:
            max = i - 1

    return -1


def search_interpolation(numbers, value):
    min = 0
    max = len(numbers) - 1

    while min <= max:
        if value < numbers[min] or value > numbers[max]:
            return -1

        pos = int(min + (max - min) * (value - numbers[min]) / (numbers[max] - numbers[min]))

        if numbers[pos] == value:
            return pos

        if numbers[pos] < value:
            min = pos + 1
        else:
            max = pos - 1

        if numbers[min] == numbers[max] == value:
            return min

    return -1


def time_func(func):
    start = time.perf_counter()
    ret = func()
    end = time.perf_counter()
    return ret, end - start


def list_init_random(l, n, z):
    l.clear()
    for i in range(0, n):
        l.append(random.randint(0, z))


if len(sys.argv) < 4:
    print("arguments missing: <length> <range> <search>")
    sys.exit()

length = int(sys.argv[1])
value_range = int(sys.argv[2])
value = int(sys.argv[3])


numbers = []

_, duration = time_func(lambda: list_init_random(numbers, length, value_range))
print("init array with random numbers in %4.6f s" % duration)

index, duration = time_func(lambda: search_linear(numbers, value))
print("linear search: search for %d in %4.6f s" % (value, duration))

_, duration = time_func(lambda: numbers.sort())
print("array sort in %4.6f s" % duration)

index, duration = time_func(lambda: search_binary(numbers, value))
print("binary search: search for %d in %4.6f s" % (value, duration))

index, duration = time_func(lambda: search_interpolation(numbers, value))
print("interpolation search: search for %d in %4.6f s" % (value, duration))
