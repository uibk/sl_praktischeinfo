import random
import time
import sys


def sort_bubble(numbers):
    length = len(numbers)

    for i in range(length):
        for j in range(length - i - 1):
            if numbers[j] > numbers[j+1]:
                numbers[j], numbers[j+1] = numbers[j+1], numbers[j]


def sort_bucket(arr, num_buckets, func):
    buckets = [list() for _ in range(num_buckets)]

    for v in arr:
        buckets[int(func(v)*(num_buckets - 1))].append(v)  # find bucket, here: equal subdivision of the value range

    arr.clear()
    for bucket in buckets:
        sort_bubble(bucket)  # bucket.sort()
        arr.extend(bucket)


def time_func(func):
    start = time.perf_counter()
    ret = func()
    end = time.perf_counter()
    return ret, end - start


def list_init_random(l, n, z):
    l.clear()
    for i in range(0, n):
        l.append(random.randint(0, z))


if len(sys.argv) < 3:
    print("arguments missing: <length> <range>")
    sys.exit()

length = int(sys.argv[1])
value_range = int(sys.argv[2])
buckets = 1000  # currently hardcoded bucketsize

numbers_a = []

_, duration = time_func(lambda: list_init_random(numbers_a, length, value_range))
print("init array with random numbers in %4.6f s" % duration)

numbers_b = numbers_a[:]

_, duration = time_func(lambda: sort_bubble(numbers_a))
print("bubblesort: %4.6f s" % duration)

_, duration = time_func(lambda: sort_bucket(numbers_b, buckets, lambda x: (x/value_range)))
print("bucketsort: %4.6f s" % duration)
